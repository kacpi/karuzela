//
//  TestViewController.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

let KARUZELA_HEIGHT: CGFloat = 210

class TestViewController: ViewController {
    
    static var GLOBAL_TABLE_VIEW_OFFSET: CGFloat = -KARUZELA_HEIGHT - 44
    
    @IBOutlet weak var tableView: IntrinsicTableView!
    
    var changeBarPosition: ((_ value: CGFloat)->())?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.contentOffset.y = TestViewController.GLOBAL_TABLE_VIEW_OFFSET
    }
    
    override func didRefreshed() {
        delay(1.0) {
            self.endRefreshing()
        }
    }
}

//

extension TestViewController: UITableViewDelegate, UITableViewDataSource {
    fileprivate func initTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.contentInset = UIEdgeInsets(top: KARUZELA_HEIGHT + 44, left: 0, bottom: 0, right: 0)
        
        self.addPrivateRefreshControl()
    }
    
    fileprivate func addPrivateRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.tag = REFRESH_CONTROL_TAG
        
        refreshControl.addTarget(self, action: #selector(self.didRefreshed), for: UIControl.Event.valueChanged)
        
        refreshControl.bounds = CGRect(x: refreshControl.bounds.origin.x, y: refreshControl.bounds.origin.y - KARUZELA_HEIGHT - 44, width: refreshControl.bounds.width, height: refreshControl.bounds.height)
        
        self.tableView.refreshControl = refreshControl
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellId")
        cell.textLabel?.text = "Row \(indexPath.row + 1)"
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.changeBarPosition?(44 + KARUZELA_HEIGHT + scrollView.contentOffset.y)
        
        if scrollView.contentOffset.y != -254.0 {
            TestViewController.GLOBAL_TABLE_VIEW_OFFSET = scrollView.contentOffset.y
        }
    }
    
    
}

// needed for sticky header

class IntrinsicTableView: UITableView {
    
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + KARUZELA_HEIGHT)
    }
    
}
