//
//  SongsListViewController.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 17/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

var GLOBAL_OFFSET: CGFloat?

class SongsListViewController: UITableViewController {
    
    var setOffset: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addPrivateRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setOffset = false
        tableView.contentInset = UIEdgeInsets(top: 254, left: 0, bottom: 0, right: 0)
        setOffset = true
        
        if let GLOBAL_OFFSET_ = GLOBAL_OFFSET {
            if GLOBAL_OFFSET_ <= -44.0 {
                tableView.setContentOffset(CGPoint(x: 0.0, y: GLOBAL_OFFSET_), animated: false)
            } else {
                if tableView.contentOffset.y < -44.0 { // scroll do zera
                    tableView.setContentOffset(CGPoint(x: 0.0, y: -44.0), animated: false)
                }
            }
        }
    }
    
    //
    
    fileprivate func addPrivateRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.tag = REFRESH_CONTROL_TAG
        
        refreshControl.addTarget(self, action: #selector(self.didRefreshed2), for: UIControl.Event.valueChanged)
        
        refreshControl.bounds = CGRect(x: refreshControl.bounds.origin.x, y: refreshControl.bounds.origin.y - KARUZELA_HEIGHT - 44, width: refreshControl.bounds.width, height: refreshControl.bounds.height)
        
        tableView.refreshControl = refreshControl
    }
    
    @objc func didRefreshed2() {
        delay(1.0) {
            self.endRefreshing()
        }
    }
    
    //
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = "Row \(indexPath.row + 1)"
        return cell
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        EventSubscriber.publish(tag: "SongListScrolled", extraData: scrollView.contentOffset.y)
        
        if setOffset {
            GLOBAL_OFFSET = scrollView.contentOffset.y
        }
    }
    
}
