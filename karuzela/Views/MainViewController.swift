//
//  ViewController.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit
import FSPagerView

class MainViewController: SwipePagesViewController {
    
    @IBOutlet weak var rootContainerView: UIView!
    @IBOutlet weak var karuzelaTopCnst: NSLayoutConstraint!
    @IBOutlet weak var karuzelaView: FSPagerView! {
        didSet {
            self.karuzelaView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    //
    
    fileprivate var oldKaruzelaTopCnst: CGFloat?
    
    fileprivate var subscribers = [Any]()
    
    fileprivate var isOldCnstSet: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTabBar()
        
        self.setupCarousel()
        
        // fetch and add tabs
        
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        self.addPage(viewController: SongsListViewController())
        
        //
        
        subscribers.append(EventSubscriber.subscribe(tag: "SongListScrolled", callback: { [weak self] (offset_) in
            if let offset = offset_ as? CGFloat {
                self?.setupChangePosition(offset)
            }
        }))
    }
    
    deinit {
        print("DEINIT MainViewController")
        
        subscribers.forEach { (subscriber) in
            EventSubscriber.unsubscribe(subscriber: subscriber)
        }
    }
    
    //
    
    fileprivate func setupTabBar() {
        // COLORS
        self.underlineColor = UIColor.white
        self.tabBarTintColor = UIColor.white
        self.tabBarBackgroundColor = .black // UIColor.mainBlueHex
        
        self.configure(mainView: self.rootContainerView)
        if UIDevice.isIphoneX {
            self.configureTabBar(titles: ["Pop", "Rock", "Polskie", "Muzyka filmowa", "Dla dzieci", "Disco polo", "Lata 90", "Miłość"], titleTopInset: 16.0)
        } else {
            self.configureTabBar(titles: ["Pop", "Rock", "Polskie", "Muzyka filmowa", "Dla dzieci", "Disco polo", "Lata 90", "Miłość"], titleBottomInset: -4.0, titleTopInset: -4.0)
        }
    }
    
    fileprivate func setupCarousel() {
        self.karuzelaView.delegate = self
        self.karuzelaView.dataSource = self
        
        self.karuzelaView.automaticSlidingInterval = 4
        self.karuzelaView.isInfinite = true
        self.karuzelaView.interitemSpacing = 10
        self.karuzelaView.itemSize = CGSize(width: 276, height: 144)
    }
    
    fileprivate func setupChangePosition(_ offset_: CGFloat) {
        let offset = offset_ + 44 + KARUZELA_HEIGHT
        
        self.tabBarPosition = max(0,KARUZELA_HEIGHT - offset)
        self.tabBarView?.frame.origin.y = self.tabBarPosition
        
        self.karuzelaTopCnst.constant = max(-KARUZELA_HEIGHT, -offset)
        
        oldKaruzelaTopCnst = self.karuzelaTopCnst.constant
    }
}

//

extension MainViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 3
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: "carousel\(index+1)")
        cell.textLabel?.text = "INDEX = \(index)"
        return cell
    }
}
