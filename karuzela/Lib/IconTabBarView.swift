//
//  IconTabBarView.swift
//  MakeApp
//
//  Created by Ufos on 03.03.2016.
//  Copyright © 2016 makeapp. All rights reserved.
//

import UIKit

class IconTabBarView: TabBarView {

    // CAN BE OVERRIDEN
    func configureIconTabBar(_ images: [UIImage]) {
        self.configureTabBar(titles: [])
        
        self.tabButtons = []
        
        for i in 0 ..< images.count {
            let button = UIButton(frame: CGRect(x: CGFloat(i) * TABBAR_BUTTON_MIN_WIDTH, y: 0, width: self.buttonWidth, height: self.frame.height))
            button.setImage(images[i], for: UIControl.State())
            button.alpha = 0.5
            button.tag = i
            button.addTarget(self, action: #selector(tabButtonClicked), for: UIControl.Event.touchUpInside)
            self.addSubview(button)
            self.tabButtons.append(button)
        }
        
        self.calculateButtonsWidth()
    }
}
