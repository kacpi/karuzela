//
//  ViewController.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController { //}, NVActivityIndicatorViewable {
    
    fileprivate(set) var didAppear: Bool = false
    fileprivate(set) var willAppear: Bool = false
    fileprivate(set) var isLoaded: Bool = false
    
    // references to segue actions
    fileprivate var segueActions: [SegueAction] = []
    
    private var isLoadingViewFullScreen: Bool = false
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isLoaded = true
        
        self.hideBackTitle()
    }
    
    //
    
    // NO ANIMATION FOR ROTATE
    //    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    //        coordinator.animate(alongsideTransition: nil) { (_) in
    //            UIView.setAnimationsEnabled(true)
    //        }
    //        UIView.setAnimationsEnabled(false)
    //        super.viewWillTransition(to: size, with: coordinator)
    //    }
    
    //
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(self.willAppear == false) {
            self.viewWillFirstAppear()
        }
        
        self.willAppear = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(self.didAppear == false) {
            self.viewDidFirstAppear()
        }
        
        self.didAppear = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // adjust loading view size
        let keyWindow = UIApplication.shared.keyWindow
        if isLoadingViewFullScreen && keyWindow != nil {
            //            self.loadingView.frame = keyWindow!.frame
        } else {
            //            self.loadingView?.frame = self.view.bounds
        }
    }
    
    //
    
    func viewWillFirstAppear() {
        // to be implemented
    }
    
    func viewDidFirstAppear() {
        // to be implemented
    }
    
    
    // add segue action
    func performSegue(_ identifier: String, withCompletion completion: @escaping ((_ destVc: UIViewController)->())) {
        self.segueActions.append(SegueAction(id: identifier, withAction: completion))
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    // use segue action
    // REMEMBER TO USE SUPER IN DESCENT VC!!!!!!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        for segueAction in self.segueActions {
            if(segue.identifier == segueAction.id) {
                segueAction.action(segue.destination)
            }
        }
    }
    
    //
    
    func hideBackTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
}


//

class SegueAction {
    fileprivate(set) var id: String
    fileprivate(set) var action: ((_ destVc: UIViewController)->())
    
    init(id: String, withAction action: @escaping ((_ destVc: UIViewController)->())) {
        self.id = id
        self.action = action
    }
}

//

let REFRESH_CONTROL_TAG: Int = 665

extension UIViewController {
    
    func removeChildrenVcs() {
        for i in 0 ..< self.children.count {
            let child : UIViewController = self.children[i]
            child.removeAsChild()
        }
    }
    
    func removeAsChild() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @discardableResult
    func addRefreshControl(_ scrollView : UIScrollView) -> UIRefreshControl
    {
        let refreshControl = UIRefreshControl()
        refreshControl.tag = REFRESH_CONTROL_TAG
        
        let color: UIColor
        color = UIColor(red: 168/255.0, green: 168/255.0, blue: 168/255.0, alpha: 1.0) // on device will be 111/111/111
        
        refreshControl.tintColor = color
        
        refreshControl.addTarget(self, action: #selector(didRefreshed), for: UIControl.Event.valueChanged)
        scrollView.addSubview(refreshControl)
        
        return refreshControl
    }
    
    func endRefreshing() {
        (self.view.viewWithTag(REFRESH_CONTROL_TAG) as? UIRefreshControl)?.endRefreshing()
    }
    
    @objc func didRefreshed()
    {
        // to be overrided
    }
}
