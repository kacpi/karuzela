//
//  SwipePagesViewController.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import Foundation
import UIKit

class SwipePagesViewController : ViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate, TabBarDelegate {
    
    let TABBAR_ORIGINAL_HEIGHT: CGFloat = 44
    let TABBAR_IPHONE_X_HEIGHT: CGFloat = 68
    
    //
    
    var viewControllerArray:[UIViewController] = []
    
    weak var swipePagesDelegate: SwipePagesDelegate?
    
    private weak var mainView_: UIView!
    
    private(set) var currentPageIndex: Int = 0
    private(set) var isScrollingToPos: Bool = false
    
    private var pageViewController: UIPageViewController!
    var tabBarView: TabBarView?
    
    //
    
    @IBInspectable var bottomOffset: CGFloat = 0
    @IBInspectable var topOffset: CGFloat = 0
    
    var tabBarPosition: CGFloat = 0
    
    @IBInspectable var underlineColor: UIColor?
    @IBInspectable var tabBarTintColor: UIColor?
    @IBInspectable var tabBarBackgroundColor: UIColor?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView_ = self.view
    }
    
    //
    
    func configure(mainView: UIView) {
        self.mainView_ = mainView
        
        // add PageVC and set delegates
        self.pageViewController = UIPageViewController(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        self.addChild(self.pageViewController )
        self.mainView_.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParent: self)
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self // DISABLE SWIPE GESTURE
        
        self.pageViewController.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pageViewController.setViewControllers([viewControllerArray[self.currentPageIndex]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
    }
    
    // TABBAR
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var newTabBarHeight: CGFloat = 0
        
        if UIDevice.isIphoneX {
            newTabBarHeight = TABBAR_IPHONE_X_HEIGHT
        } else {
            newTabBarHeight = TABBAR_ORIGINAL_HEIGHT
        }
        
        if let tabBar = self.tabBarView {
            tabBar.frame = CGRect(x: -15, y: tabBarPosition, width: self.mainView_.bounds.width + 15, height: newTabBarHeight)
            tabBar.calculateButtonsWidth()
            self.pageViewController.view.frame = CGRect(x: 0, y: self.topOffset, width: self.mainView_.bounds.width, height: self.mainView_.bounds.height - newTabBarHeight - bottomOffset)
        } else {
            self.pageViewController.view.frame = CGRect(x: 0, y: self.topOffset, width: self.mainView_.bounds.width, height: self.mainView_.bounds.height-bottomOffset)
        }
    }
    
    // this allows us to get information back from the scrollview, namely the coordinate information that we can link to the selection bar.
    private func syncScrollView() {
        for view in self.pageViewController.view.subviews {
            (view as? UIScrollView)?.delegate = self
            (view as? UIScrollView)?.delaysContentTouches = false
        }
    }
    
    // user
    
    func addPage(viewController: UIViewController) {
        self.viewControllerArray.append(viewController)
    }
    
    func configureTabBar(titles: [String], height: CGFloat = 45, titleBottomInset: CGFloat = 0, titleTopInset: CGFloat = 0) {
        self.tabBarView = TabBarView()
        self.tabBarView?.configureTabBar(titles: titles, titleBottomInset: titleBottomInset, titleTopInset: titleTopInset)
        self.tabBarView?.tabBarDelegate = self
        self.tabBarView?.isPageVc = true
        self.tabBarView?.tintColor = self.tabBarTintColor
        self.tabBarView?.underlineColor = self.underlineColor
        self.tabBarView?.backgroundColor = self.tabBarBackgroundColor
        self.mainView_.addSubview(self.tabBarView!)
        
        self.syncScrollView()
    }
    
    func configureIconTabBar(images: [UIImage]) {
        self.tabBarView = IconTabBarView()
        (self.tabBarView as? IconTabBarView)?.configureIconTabBar(images)
        self.tabBarView?.tabBarDelegate = self
        self.tabBarView?.isPageVc = true
        self.tabBarView?.tintColor = self.tabBarTintColor
        self.tabBarView?.underlineColor = self.underlineColor
        self.tabBarView?.backgroundColor = self.tabBarBackgroundColor
        self.mainView_.addSubview(self.tabBarView!)
        
        self.syncScrollView()
    }
    
    
    func scrollToPage(index: Int) {
        self.isScrollingToPos = true
        if(index > self.currentPageIndex) {
            self.pageViewController.setViewControllers([viewControllerArray[index]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: { (completed) -> Void in
                self.isScrollingToPos = false
                if(completed) {
                    self.currentPageIndex = index
                    //self.tabBarView?.selectTab(self.currentPageIndex)
                    self.swipePagesDelegate?.pageDidShow(index: self.currentPageIndex)
                }
            })
        } else {
            self.pageViewController.setViewControllers([viewControllerArray[index]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: { (completed) -> Void in
                self.isScrollingToPos = false
                if(completed) {
                    self.currentPageIndex = index
                    //self.tabBarView?.selectTab(self.currentPageIndex)
                    self.swipePagesDelegate?.pageDidShow(index: self.currentPageIndex)
                }
            })
        }
    }
    
    func gotoPage(index: Int) {
        self.tabBarView?.selectTab(index: index)
        self.scrollToPage(index: index)
    }
    
    func tabClicked(index: Int) {
        self.scrollToPage(index: index)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // !self.isScrollingToPos to avoid double scrolling on tabbar
        if(self.tabBarView != nil && !self.isScrollingToPos) {
            self.tabBarView?.contentDidScroll(scrollPos: scrollView.contentOffset.x, scrollWidth: scrollView.bounds.width)
        }
    }
    
    
    // MARK: Page View Controller Data Source
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index :Int = self.indexOfController(viewController: viewController);
        if (index == NSNotFound) {
            return nil
        }
        index -= 1
        if (0 <= index && index < viewControllerArray.count) {
            return viewControllerArray[index]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index :Int = self.indexOfController(viewController: viewController)
        if (index == NSNotFound) {
            return nil
        }
        index += 1
        if (0 <= index && index < viewControllerArray.count) {
            return viewControllerArray[index]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.currentPageIndex = self.indexOfController(viewController: pageViewController.viewControllers!.first! as UIViewController)
        self.tabBarView?.selectTab(index: self.currentPageIndex)
        self.swipePagesDelegate?.pageDidShow(index: self.currentPageIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed) {
            self.currentPageIndex = self.indexOfController(viewController: pageViewController.viewControllers!.last! as UIViewController)
            self.tabBarView?.selectTab(index: self.currentPageIndex)
            self.swipePagesDelegate?.pageDidShow(index: self.currentPageIndex)
        }
    }
    
    //%%% checks to see which item we are currently looking at from the array of view controllers.
    func indexOfController(viewController :UIViewController) -> Int {
        for i in 0  ..< viewControllerArray.count {
            if (viewController == viewControllerArray[i]) {
                return i
            }
        }
        return NSNotFound
    }
}

//

protocol SwipePagesDelegate: class {
    func pageDidShow(index: Int)
}

//

class AddPageSegue: UIStoryboardSegue {
    
    override func perform() {
        let swipePagesViewController: SwipePagesViewController = self.source as! SwipePagesViewController
        let destinationViewController: UIViewController = self.destination
        
        swipePagesViewController.viewControllerArray.append(destinationViewController)
    }
}
