//
//  Colors.swift
//  Shareink
//
//  Created by ufos on 05.07.2015.
//  Copyright (c) 2015 ufos. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    //
    convenience init(r: Int, g: Int, b: Int, a: CGFloat = 1) {
        self.init(red: CGFloat(r)/CGFloat(255), green: CGFloat(g)/CGFloat(255), blue: CGFloat(b)/CGFloat(255), alpha: a)
    }
    
    convenience init(hex: String) {
        if (hex.count < 6) {
            self.init(r: 200, g: 200, b: 200)
            return
        }
        
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    
    // iSingPlayer
    
    static var mainBlue: UIColor {
        return UIColor(r: 13, g: 107, b: 255) // before - 15,110,208
    }
    
    static var mainBlueHex: UIColor {
        return UIColor(hex: "#0D6BFF")
    }
    
    static var graySearchMedium: UIColor {
        return UIColor(r: 225, g: 225, b: 225)
    }
    
    static var graySearch: UIColor {
        return UIColor(r: 237, g: 237, b: 237)
    }
    
    static var graySearchDark: UIColor {
        return UIColor(hex: "#d6d6d6")
    }
    
    static var mainOrange: UIColor {
        return UIColor(hex: "#F67C25")
    }
    
    static var secondBlue: UIColor {
        return UIColor(hex: "#2EB5F1")
    }

    static var backgroundGray: UIColor {
        return UIColor(hex: "#252525")
    }

    static var sliderLineBackground: UIColor {
        return UIColor(hex: "#CFCFCF")
    }
    
    static var selectGray: UIColor {
        return UIColor(hex: "#EFEFEF")
    }
    
    static var newYellow: UIColor {
        return UIColor(hex: "#fde14b")
    }
    
    //
    
    static var graySongTime: UIColor {
        return UIColor(hex: "#c3c1c4")
    }
    
    static var newBlue: UIColor {
        return UIColor(hex: "#139ed9")
    }
    
    // NEW COLORS
    
    static var grayLabel: UIColor {
        return UIColor(r: 144, g: 144, b: 144)
    }
    
    static var grayBorder: UIColor {
        return UIColor(r: 197, g: 197, b: 197)
    }
    
    static var navBarShadow: UIColor {
        return UIColor(r: 212, g: 212, b: 212)
    }
}
