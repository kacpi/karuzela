//
//  Helper.swift
//  ising
//
//  Created by Ufos on 26.05.2017.
//  Copyright © 2017 Panowie-Programisci. All rights reserved.
//

import Foundation
import UIKit

//

func isIPAD () -> Bool { return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad }
func isIPhone() -> Bool { return !isIPAD() }

//

func log(_ string: String) {
    NSLog(string)
}

// time in seconds
func delay(_ time: TimeInterval, withCompletion completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time, execute: { () -> Void in
        completion()
    })
}

// safe [0] checking
extension Array {
    subscript (safe index: Int) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

//

func stringForKey(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

//

extension CGRect {
    func resize(_ tw: CGFloat, th: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x, y: self.origin.y, width: self.size.width + tw, height: self.size.height + th)
    }
    
    func rectWithHeight(_ height: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x, y: self.origin.y, width: self.size.width, height: height)
    }
    
    func move(_ dx: CGFloat, dy: CGFloat) -> CGRect{
        return CGRect(x: self.origin.x + dx, y: self.origin.y + dy, width: self.size.width, height: self.size.height)
    }
    
    func setX(_ x: CGFloat) -> CGRect{
        return CGRect(x: x, y: self.origin.y, width: self.size.width, height: self.size.height)
    }
}

//

extension UIImage {

    static func createCircle(color: UIColor, radius: CGFloat) -> UIImage? {
        let image = UIImage(color: color)!
        
        let width = radius*2
        let scale = width / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: width, height: newHeight))
        
        UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: width, height: newHeight)).addClip()
        
        image.draw(in: CGRect(x: 0, y: 0, width: width, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}


//

extension UISearchBar {
    
    // I cannot understand why it's not automatically built in
    func setCancelText(text: String) {
        self.cancelButton?.setTitle(text, for: .normal)
    }
    
    fileprivate var cancelButton: UIButton? {
        
        for sv in self.subviews {
            if let sv_ = sv as? UIButton {
                return sv_
            }
            
            for chilsv in sv.subviews {
                if let sv_ = chilsv as? UIButton {
                    return sv_
                }
            }
        }
        
        return nil
    }
    
}

//


extension Int {

    var timeString: String {
        let m = self / 60
        let s = self % 60
        
        return "\(m):" + ((s < 10) ? "0" : "") + "\(s)"
    }
}

//

var statusBarView: UIView? {
    return UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
}

func setStatusBarBackgroundColor(color: UIColor? = nil) {
    guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
    
    statusBar.backgroundColor = color
}

//

func stringToDate(_ string: String) -> Date {
    let dateFormatter = ISO8601DateFormatter()
    let date = Date()
    
    if let dateFromString = dateFormatter.date(from: string) {
        return dateFromString
    }
    
    return date
}

//

func sizeForLocalFilePath(filePath: String) -> UInt64 {
    do {
        let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
        if let fileSize = fileAttributes[FileAttributeKey.size]  {
            return (fileSize as! NSNumber).uint64Value
        } else {
            print("Failed to get a size attribute from path: \(filePath)")
        }
    } catch {
        print("Failed to get file attributes for local path: \(filePath) with error: \(error)")
    }
    return 0
}

