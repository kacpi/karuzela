//
//  TabBarView.swift
//  karuzela
//
//  Created by Kacper Piątkowski on 11/12/2018.
//  Copyright © 2018 Kacper Piątkowski. All rights reserved.
//

import Foundation
import UIKit

class TabBarView: UIScrollView {
    
    let TABBAR_UNDERLINE_HEIGHT: CGFloat = 4
    let TABBAR_LINE_HEIGHT: CGFloat = 1
    @IBInspectable var TABBAR_BUTTON_MIN_WIDTH: CGFloat = 100
    
    
    private(set) var currentPageIndex: Int = 0
    var tabButtons: [UIButton]!
    var tabButtonsWidth: [CGFloat] = []
    private var underlineView: UIView!
    private var lineView: UIView!
    
    private(set) var buttonWidth: CGFloat = 0
    
    // different approach for PageViewController
    var isPageVc: Bool = false
    
    //
    
    
    var notSelectedColor: UIColor = UIColor.white.withAlphaComponent(0.6) {
        didSet {
            for button in self.tabButtons {
                button.setTitleColor(self.notSelectedColor, for: UIControl.State.normal)
            }
        }
    }
    
    var underlineColor: UIColor! = UIColor.black {
        didSet {
            self.underlineView?.backgroundColor = self.underlineColor
        }
    }
    
    private var underlineColor_: UIColor! {
        get {
            if self.underlineColor == nil {
                return self.tintColor
            }
            return self.underlineColor
        }
    }
    
    //
    
    weak var tabBarDelegate: TabBarDelegate?
    
    var buttonFont: UIFont = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular) {
        didSet {
            for button in self.tabButtons {
                button.titleLabel?.font = self.buttonFont
            }
        }
    }
    
    var buttonSelectedFont: UIFont = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
    
    //
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        self.calculateButtonsWidth()
        
        self.delaysContentTouches = false
        self.canCancelContentTouches = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.tabButtons = []
        //self.tabButtonsWidth = []
        self.clipsToBounds = true
        
        self.delaysContentTouches = false
    }
    
    
    // FOR USER
    
    func selectTab(index: Int, animated: Bool = true) {
        self.currentPageIndex = index
        self.setButtonSelected(buttonIndex: self.currentPageIndex)
        
        let subarrayWidth: ArraySlice<CGFloat> = self.tabButtonsWidth[0..<self.currentPageIndex]
        let sumSubarrayWidth = subarrayWidth.reduce(0, +)
        let underlinePos = sumSubarrayWidth // CGFloat(self.currentPageIndex*Int(self.tabButtonsWidth[index]))
        if (animated) {
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.underlineView.frame = CGRect(x: underlinePos + 30, y: self.frame.height-self.TABBAR_UNDERLINE_HEIGHT, width: self.tabButtonsWidth[self.currentPageIndex] - 12, height: self.TABBAR_UNDERLINE_HEIGHT)
                //self.underlineView.frame = self.underlineView.frame.setX(underlinePos)
                // to make underline always visible
                self.contentOffset.x = min(max(underlinePos - self.frame.width/3, 0), self.contentSize.width - self.frame.width)
            }
        } else {
            self.underlineView.frame = self.underlineView.frame.setX(underlinePos + 30)
            // to make underline always visible
            self.contentOffset.x = min(max(underlinePos - self.frame.width/3, 0), self.contentSize.width - self.frame.width)
        }
    }
    
    func alignToCenter() {
        let viewWidth =  self.frame.width
//        let contentWidth = self.buttonWidth * CGFloat(self.tabButtons.count)
        let contentWidth = self.tabButtonsWidth.reduce(0, +)
        
        self.contentOffset = CGPoint(x: (max(0, contentWidth - viewWidth)) / 2, y: 0)
    }
    
    //
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.calculateButtonsWidth()
    }
    
    // when content is scrolling
    func contentDidScroll(scrollPos: CGFloat, scrollWidth: CGFloat) {
        
        // calculate underlineView x-pos
        let diff = scrollPos - scrollWidth
        var underlinePos: CGFloat = 0
        
        if(self.isPageVc == true) {
            let subarrayWidth: ArraySlice<CGFloat> = self.tabButtonsWidth[0..<self.currentPageIndex]
            let sumSubarrayWidth = subarrayWidth.reduce(0, +)
            underlinePos = sumSubarrayWidth + (diff * (self.tabButtonsWidth[self.currentPageIndex]/scrollWidth))
            
            // if mnore than half scrolled -> setButtonSelected
            let diffDiv = diff/scrollWidth
            
            if(diffDiv > 0) {
                if(diffDiv > 0.5 && self.tabButtons.count > self.currentPageIndex+1) {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex+1)
                } else {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex)
                }
            } else {
                if(diffDiv < -0.5 && self.currentPageIndex > 0) {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex-1)
                } else {
                    self.setButtonSelected(buttonIndex: self.currentPageIndex)
                }
            }
        }
            // if PageVC then scrollPos is calculated from current tabs pos
        else {
            let subarrayWidth: ArraySlice<CGFloat> = self.tabButtonsWidth[0..<self.currentPageIndex]
            let sumSubarrayWidth = subarrayWidth.reduce(0, +)
            underlinePos = sumSubarrayWidth + diff * (self.tabButtonsWidth[self.currentPageIndex]/scrollWidth)
            self.currentPageIndex = min(self.tabButtons.count-1, Int((scrollPos+(scrollWidth/2)) / scrollWidth))
            self.setButtonSelected(buttonIndex: self.currentPageIndex)
        }
        
        self.underlineView.frame = self.underlineView.frame.setX(underlinePos + 30)
    }
    
    // if possible, align all buttons, else use min width
    func calculateButtonsWidth() {
        if(self.tabButtons != nil && self.tabButtons.count > 0) {
//            let fitWidth = self.frame.width / CGFloat(self.tabButtons.count)
            // self.buttonWidth = max(TABBAR_BUTTON_MIN_WIDTH, fitWidth)
            
            
            for i in 0 ..< self.tabButtons.count {
                let subarrayWidth: ArraySlice<CGFloat> = self.tabButtonsWidth[0..<i]
                let sumSubarrayWidth = subarrayWidth.reduce(0, +)
                self.tabButtons[i].frame = CGRect(x: sumSubarrayWidth + 24, y: 0, width: self.tabButtonsWidth[i], height: self.frame.height)
            }
            
            let widthButtonsSum = self.tabButtonsWidth.reduce(0, +)
            self.contentSize = CGSize(width: widthButtonsSum, height: self.frame.height)
            
            self.setButtonSelected(buttonIndex: self.currentPageIndex)
            
            let subarrayWidth: ArraySlice<CGFloat> = self.tabButtonsWidth[0..<self.currentPageIndex]
            let sumSubarrayWidth = subarrayWidth.reduce(0, +)
            
            self.underlineView.frame = CGRect(x: sumSubarrayWidth + 30, y: self.frame.height-TABBAR_UNDERLINE_HEIGHT, width: self.tabButtonsWidth[self.currentPageIndex] - 12, height: TABBAR_UNDERLINE_HEIGHT)
            
            self.lineView.frame = CGRect(x: 0, y: self.bounds.height - TABBAR_LINE_HEIGHT, width: self.bounds.width, height: TABBAR_LINE_HEIGHT)
        }
    }
    
    // if not called there is no tabbar
    func configureTabBar(titles: [String], titleBottomInset: CGFloat = 0, titleTopInset: CGFloat = 0) {
        // configure TabBar
        self.bounces = false
        self.isScrollEnabled = true
        self.alwaysBounceHorizontal = true
        self.showsHorizontalScrollIndicator = false
        
        if(self.lineView == nil) {
            self.lineView = UIView()
            //            self.lineView.backgroundColor = UIColor.init(hex: "e8e8e8")
            self.lineView.backgroundColor = UIColor.mainBlueHex
            
            self.addSubview(self.lineView)
        }
        
        if(self.underlineView == nil) {
            self.underlineView = UIView()
            self.underlineView.backgroundColor = self.underlineColor_
            self.addSubview(self.underlineView)
        }
        
        self.tabButtons = []
        
        for i in 0 ..< titles.count {
            let button = UIButton(type: UIButton.ButtonType.system)
            button.frame = CGRect(x: 0, y: 0, width: button.frame.width, height: self.frame.height)
            button.setTitle(titles[i], for: UIControl.State.normal)
            button.titleLabel?.addKern(kernValue: -0.27)
            button.setTitleColor(self.tintColor, for: UIControl.State.normal)
            //            button.setTitleColor(self.notSelectedColor.withAlphaComponent(0.3), for: .highlighted)
            button.titleLabel?.font = self.buttonFont
            button.tag = i
            button.titleEdgeInsets = UIEdgeInsets(top: titleTopInset, left: 0, bottom: titleBottomInset, right: 0)
            button.addTarget(self, action: #selector(tabButtonClicked), for: UIControl.Event.touchUpInside)
            button.sizeToFit()
            self.addSubview(button)
            self.tabButtons.append(button)
            self.tabButtonsWidth.append(button.frame.width + 24 - 3)
        }
        
        self.calculateButtonsWidth()
    }
    
    @objc func tabButtonClicked(button: UIButton) {
        self.selectTab(index: button.tag)
        self.tabBarDelegate?.tabClicked(index: button.tag)
    }
    
    private func setButtonSelected(buttonIndex: Int) {
        for button in self.tabButtons {
            button.setTitleColor(self.notSelectedColor, for: .normal)
            //            button.setTitleColor(self.notSelectedColor.withAlphaComponent(0.3), for: .highlighted)
            
            button.titleLabel?.font = self.buttonFont
        }
        
        self.tabButtons[buttonIndex].setTitleColor(self.tintColor, for: .normal)
        //        self.tabButtons[buttonIndex].setTitleColor(self.tintColor.withAlphaComponent(0.3), for: .highlighted)
        self.tabButtons[buttonIndex].titleLabel?.font = self.buttonSelectedFont
    }
}


//

protocol TabBarDelegate: class {
    func tabClicked(index: Int)
}
