//
//  Extensions.swift
//  ising
//
//  Created by Kacper Piątkowski on 24.05.2017.
//  Copyright © 2017 Panowie-Programisci. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}


//

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}


//


extension UIViewController {
    func setTitleImage(image: UIImage, width: CGFloat=100, height: CGFloat=44) {
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))

        imgView.contentMode = .scaleAspectFit
        imgView.image = image
        
        self.navigationItem.titleView = imgView
    }
}

// https://stackoverflow.com/questions/46192280/detect-if-the-device-is-iphone-x

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        // include iPhone XS, XS Max and XR, simply look for models starting with "iPhone11,"
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6" || modelIdentifier.starts(with: "iPhone11,")
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    static var isIphone4InchFamily: Bool { // iPhone 5, 5s, 5c, SE
        return UIScreen.main.nativeBounds.size.height == 1136
    }
    
    static var isIphone4_7InchFamily: Bool { // iPhone 6, 7, 8
        return UIScreen.main.nativeBounds.size.height == 1334
    }
    
    static var isIphone5_5InchFamily: Bool { // iPhone 6+, 7+, 8+
        return UIScreen.main.nativeBounds.size.height == 2208
    }
}

// navigate through textfields (next/done buttons)

extension UITextField {
    class func connectFields(fields:[UITextField]) -> Void {
        guard let last = fields.last else {
            return
        }
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .done
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
}

//

extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

//

extension UINavigationBar {
    func setUserBackgroundColor(_ color: UIColor = .white) {
        self.isTranslucent = false
        self.shadowImage = UIColor.clear.as1ptImage()
        self.setBackgroundImage(color.as1ptImage(), for: .default)
    }
    
    func hideBottomBorder() {
        self.shadowImage = UIColor.clear.as1ptImage()
    }
    
    func addShadow(height: CGFloat = 1.0, radius: CGFloat = 5.0) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
    }
}

//

extension UISearchBar {
    var textField: UITextField? {
        let textField = subviews.first?.subviews.first(where: { $0.isKind(of: UITextField.self) }) as? UITextField
        return textField
    }
}

//

extension UIViewController {
    func hideNavigationBar(_ animated: Bool = true) {
        // hide navigation bar on this vc
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func showNavigationBar(_ animated: Bool = true) {
        // show navigation bar on others vc
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

// https://stackoverflow.com/questions/9450302/get-uiscrollview-to-scroll-to-the-top

extension UIScrollView {
    func scrollToTop(_ topScroll: CGFloat? = nil, animated: Bool = true) {
        var desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        if let topScroll_ = topScroll {
            desiredOffset = CGPoint(x: 0, y: topScroll_)
        }
        
        self.setContentOffset(desiredOffset, animated: animated)
    }
}

// https://stackoverflow.com/questions/5084845/how-to-set-the-opacity-alpha-of-a-uiimage

extension UIImage {
    func alpha(_ alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

// https://stackoverflow.com/questions/16608536/how-to-get-the-previous-viewcontroller-that-pushed-my-current-view

extension UIViewController {
    var previousViewController: UIViewController? {
        if let controllersOnNavStack = self.navigationController?.viewControllers {
            let n = controllersOnNavStack.count
            //if self is still on Navigation stack
            if controllersOnNavStack.last === self, n > 1 {
                return controllersOnNavStack[n - 2]
            } else if n > 0{
                return controllersOnNavStack[n - 1]
            }
        }
        return nil
    }
}

//

extension UIRefreshControl {
    func refreshManually() {
        beginRefreshing()
        sendActions(for: .valueChanged)
    }
}

// https://stackoverflow.com/questions/40991450/ios-present-uialertcontroller-on-top-of-everything-regardless-of-the-view-hier

public extension UIViewController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}

// https://stackoverflow.com/questions/16801948/fade-between-two-uibutton-images

extension UIButton {
    
    func setImage(_ image: UIImage?, for state: UIControl.State, animated: Bool) {
        guard animated, let oldImage = imageView?.image, let newImage = image else {
            // Revert to default functionality
            setImage(image, for: state)
            return
        }
        
        let crossFade = CABasicAnimation(keyPath:"contents")
        crossFade.duration = 0.35
        crossFade.fromValue = oldImage.cgImage
        crossFade.toValue = newImage.cgImage
        crossFade.isRemovedOnCompletion = false
        imageView?.layer.add(crossFade, forKey: "animateContents")
        
        setImage(image, for: state)
    }
}

// https://stackoverflow.com/questions/28108745/how-to-find-actual-number-of-lines-of-uilabel

extension UILabel {
    func calculateMaxLines(itemWidth: CGFloat? = nil) -> Int {
        let itemWidth_ = itemWidth ?? frame.size.width
        
        let maxSize = CGSize(width: itemWidth_, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let lines = Int(textSize.height/charSize)
        return lines
    }
}

// https://stackoverflow.com/questions/34269399/how-to-control-shadow-spread-and-blur

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}

//

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}
