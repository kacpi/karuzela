//
//  UILabel+Extensions.swift
//  ising
//
//  Created by Kacper Piątkowski on 02/01/2019.
//  Copyright © 2019 Panowie-Programisci. All rights reserved.
//

import UIKit
import Foundation

extension UILabel {
    
    /**
     Add kerning to a UILabel's existing `attributedText`
     - note: If `UILabel.attributedText` has not been set, the `UILabel.text`
     value will be returned from `attributedText` by default
     - note: This method must be called each time `UILabel.text` or
     `UILabel.attributedText` has been set
     - parameter kernValue: The value of the kerning to add
     */
    func addKern(kernValue: CGFloat, lineHeightMultiple: CGFloat = 1.0) {
        guard let attributedText = attributedText,
            attributedText.string.count > 0,
            let fullRange = attributedText.string.range(of: attributedText.string) else {
                return
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 0.0 // values 1.0 to 5.0
        paragraphStyle.lineHeightMultiple = lineHeightMultiple // values 0.5 to 2.0
        // paragraphStyle.minimumLineHeight = 0.0
        
        let updatedText = NSMutableAttributedString(attributedString: attributedText)
        updatedText.addAttributes([.kern: kernValue, NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSRange(fullRange, in: attributedText.string))
        
        self.attributedText = updatedText
    }
}

