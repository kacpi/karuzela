//
//  EventSubscriber.swift
//  ising
//
//  Created by Kacper Piątkowski on 20.06.2018.
//  Copyright © 2018 Panowie-Programisci. All rights reserved.
//

import Foundation

class EventSubscriber {
    
    static func publish(tag: String, extraData: Any? = nil) {
        let notificationName = NSNotification.Name(rawValue: tag)
        NotificationCenter.default.post(name: notificationName, object: extraData)
    }
    
    static func subscribe(tag: String, callback: @escaping (_ extraData: Any?)->()) -> Any {
        let notificationName = NSNotification.Name(rawValue: tag)
        return NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: OperationQueue.main) { (notification) in
            callback(notification.object)
        }
    }
    
    static func unsubscribe( subscriber: Any) {
        NotificationCenter.default.removeObserver(subscriber)
    }
    
}
